using Microsoft.EntityFrameworkCore;
using News.DAL.Context;
using News.DAL.Entities.Common;

namespace News.DAL.Repositories.Common;

public class BaseRepository<TEntity, TIdentifier> : IRepository<TEntity, TIdentifier> where TEntity: IdentifiableObject<TIdentifier>
{
    protected readonly NewsDbContext DbContext;

    public BaseRepository(NewsDbContext context)
    {
        DbContext = context;
    }

    public virtual List<TEntity> GetAll()
    {
        return DbContext.Set<TEntity>().ToList();
    }

    public virtual TEntity? GetById(TIdentifier id)
    {
        return DbContext.Set<TEntity>().Find(id);
    }

    public virtual void Add(TEntity entity)
    {
        DbContext.Set<TEntity>().Add(entity);
    }

    public virtual void Delete(TEntity entity)
    {
        DbContext.Set<TEntity>().Remove(entity);
    }

    public virtual void DeleteById(TIdentifier id)
    {
        var entity = GetById(id);
        if (entity != null)
        {
            Delete(entity);
        }
    }

    public void Update(TEntity entity)
    {
        DbContext.Set<TEntity>().Attach(entity);
        DbContext.Entry(entity).State = EntityState.Modified;
    }
}