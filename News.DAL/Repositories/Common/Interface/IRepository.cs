using News.DAL.Entities.Common;

namespace News.DAL.Repositories.Common;

public interface IRepository<TEntity, in TIdentifier> where TEntity: IdentifiableObject<TIdentifier>
{
    List<TEntity> GetAll();
    TEntity? GetById(TIdentifier id);
    void Add(TEntity entity);
    void Delete(TEntity entity);
    void DeleteById(TIdentifier id);
    void Update(TEntity entity);
}