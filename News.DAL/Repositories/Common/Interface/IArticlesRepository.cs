using News.DAL.Entities;

namespace News.DAL.Repositories.Common.Interface;

public interface IArticlesRepository : IRepository<Article, int>
{
    public List<Article> GetFiltered(int? userId, string? theme, ICollection<string> tags, DateTime? fromDate, DateTime? toDate);
    public List<Article> GetByUserId(int id);
    public List<Article> GetByTheme(string id);
    public List<Article> GetByTag(string id);
    public List<Article> GetByTags(ICollection<string> ids);
}