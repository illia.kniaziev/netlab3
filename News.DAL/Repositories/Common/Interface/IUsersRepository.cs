using News.DAL.Entities;

namespace News.DAL.Repositories.Common.Interface;

public interface IUsersRepository : IRepository<User, int> { /* default */ }