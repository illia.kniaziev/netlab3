using News.DAL.Entities;

namespace News.DAL.Repositories.Common.Interface;

public interface ITagsRepository : IRepository<Tag, string> { /* default */ }