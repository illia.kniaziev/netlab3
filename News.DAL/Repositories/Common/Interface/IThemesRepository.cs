using News.DAL.Entities;

namespace News.DAL.Repositories.Common.Interface;

public interface IThemesRepository : IRepository<Theme, string> { /* default */ }