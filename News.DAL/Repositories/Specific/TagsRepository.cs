using Microsoft.EntityFrameworkCore;
using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.Repositories.Common;
using News.DAL.Repositories.Common.Interface;

namespace News.DAL.Repositories.Specific;

public class TagsRepository: BaseRepository<Tag, string>, ITagsRepository
{
    public TagsRepository(NewsDbContext context) : base(context) { /* default */ }

    public override Tag? GetById(string id)
    {
        return DbContext.Tags
            .Include(a => a.Articles)
            .FirstOrDefault(a => a.Id == id);
    }
}