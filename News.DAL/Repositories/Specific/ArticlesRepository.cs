using Microsoft.EntityFrameworkCore;
using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.Repositories.Common;
using News.DAL.Repositories.Common.Interface;

namespace News.DAL.Repositories.Specific;

public class ArticlesRepository : BaseRepository<Article, int>, IArticlesRepository
{
    public ArticlesRepository(NewsDbContext context) : base(context) { /* default */ }
    
    public override List<Article> GetAll()
    {
        return DbContext.Articles
            .Include(a => a.Tags)
            .Include(a => a.Theme)
            .Include(a => a.User)
            .ToList();
    }
    
    public override Article? GetById(int id)
    {
        return DbContext.Articles
            .Include(a => a.User)
            .FirstOrDefault(a => a.Id == id);
    }

    public List<Article> GetFiltered(int? userId, string? theme, ICollection<string>? tags, DateTime? fromDate, DateTime? toDate)
    {
        IQueryable<Article> query;
        
        if (tags is null)
        {
            query = DbContext.Articles;
        } else if (tags.Count == 0)
        {
            query = DbContext.Articles;
        }
        else
        {
            query = DbContext.Tags
                .Where(t => tags.Contains(t.Id))
                .SelectMany(t => t.Articles);
        }

        query = query
        .Include(a => a.Tags)
        .Include(a => a.Theme)
        .Include(a => a.User);

        if (fromDate is not null)
        {
            query = query.Where(a => a.CreatedAt > fromDate);
        }

        if (toDate is not null)
        {
            query = query.Where(a => a.CreatedAt < toDate);
        }
        
        if (userId is not null)
        {
            query = query.Where(a => a.UserId == userId);
        }
        
        if (theme is not null)
        {
            query = query.Where(a => a.ThemeId == theme);
        }
        
        return query.ToList();
    }

    public List<Article> GetByUserId(int id)
    {
        return DbContext.Articles
            .Include(a => a.Tags)
            .Include(a => a.Theme)
            .Include(a => a.User)
            .Where(a => a.UserId == id)
            .ToList();
    }
    
    public List<Article> GetByTheme(string id)
    {
        return DbContext.Articles
            .Include(a => a.Tags)
            .Include(a => a.Theme)
            .Include(a => a.User)
            .Where(a => a.ThemeId == id)
            .ToList();
    }
    
    public List<Article> GetByTag(string id)
    {
        return DbContext.Tags
            .Where(t => t.Id == id)
            .SelectMany(t => t.Articles)
            .Include(a => a.Tags)
            .Include(a => a.Theme)
            .Include(a => a.User)
            .ToList();
    }

    public List<Article> GetByTags(ICollection<string> ids)
    {
        return DbContext.Tags
            .Where(t => ids.Contains(t.Id))
            .SelectMany(t => t.Articles)
            .Include(a => a.Tags)
            .Include(a => a.Theme)
            .Include(a => a.User)
            .ToList();
    }
}