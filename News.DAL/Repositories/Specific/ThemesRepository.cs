using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.Repositories.Common;
using News.DAL.Repositories.Common.Interface;

namespace News.DAL.Repositories.Specific;

public class ThemesRepository : BaseRepository<Theme, string>, IThemesRepository
{
    public ThemesRepository(NewsDbContext context) : base(context) { /* default */ }
}
