using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.Repositories.Common;
using News.DAL.Repositories.Common.Interface;

namespace News.DAL.Repositories.Specific;

public class UsersRepository : BaseRepository<User, int>, IUsersRepository
{
    public UsersRepository(NewsDbContext context) : base(context) { /* default */ }
}