﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace News.DAL.Migrations
{
    /// <inheritdoc />
    public partial class MakeArticleThemeOptionalToHandleDeletion : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles");

            migrationBuilder.AlterColumn<string>(
                name: "ThemeId",
                table: "Articles",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "Id",
                onDelete: ReferentialAction.SetNull);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles");

            migrationBuilder.AlterColumn<string>(
                name: "ThemeId",
                table: "Articles",
                type: "TEXT",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Articles_Themes_ThemeId",
                table: "Articles",
                column: "ThemeId",
                principalTable: "Themes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
