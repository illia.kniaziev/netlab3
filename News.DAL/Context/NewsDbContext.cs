using Microsoft.EntityFrameworkCore;
using News.DAL.Entities;

namespace News.DAL.Context;

public class NewsDbContext : DbContext
{
    public DbSet<User> Users { get; set; }
    public DbSet<Article> Articles { get; set; }
    public DbSet<Tag> Tags { get; set; }
    public DbSet<Theme> Themes { get; set; }

    private string DbPath;
    
    public NewsDbContext()
    {
        var folder = Environment.SpecialFolder.LocalApplicationData;
        var path = Environment.GetFolderPath(folder);
        DbPath = System.IO.Path.Join(path, "news.sqlite");

        Console.WriteLine($"Database path: {DbPath}");
    }
    
    public NewsDbContext(DbContextOptions<NewsDbContext> options) : base(options) { /* default */ }
    
    protected override void OnConfiguring(DbContextOptionsBuilder options)
        => options.UseSqlite($"Data Source={DbPath}");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<User>()
            .HasMany(u => u.Articles)
            .WithOne(a => a.User)
            .HasForeignKey(a => a.UserId)
            .OnDelete(DeleteBehavior.Cascade);
        
        modelBuilder.Entity<Theme>()
            .HasMany(t => t.Articles)
            .WithOne(a => a.Theme)
            .HasForeignKey(a => a.ThemeId)
            .OnDelete(DeleteBehavior.SetNull);

        modelBuilder.Entity<Article>()
            .Property(a => a.CreatedAt)
            .HasDefaultValueSql("CURRENT_TIMESTAMP");
    }
}