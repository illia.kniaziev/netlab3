using News.DAL.Entities;
using News.DAL.Repositories.Common;
using News.DAL.Repositories.Common.Interface;
using News.DAL.Repositories.Specific;

namespace News.DAL.UnitOfWork;

public interface IUnitOfWork
{
    public UsersRepository UsersRepository { get; }
    public ArticlesRepository ArticlesRepository { get; }
    public ThemesRepository ThemesRepository { get; }
    public TagsRepository TagsRepository { get; }

    public void AutoCommit(Action block);
    
    public void Commit();
}