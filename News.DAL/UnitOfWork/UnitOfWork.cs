using News.DAL.Context;
using News.DAL.Repositories.Specific;

namespace News.DAL.UnitOfWork;

public class UnitOfWork : IUnitOfWork
{
    public UsersRepository UsersRepository { get; }
    public ArticlesRepository ArticlesRepository { get; }
    public ThemesRepository ThemesRepository { get; }
    public TagsRepository TagsRepository { get; }

    private readonly NewsDbContext _dbContext;

    public UnitOfWork(NewsDbContext context)
    {
        _dbContext = context;

        UsersRepository = new UsersRepository(context);
        ArticlesRepository = new ArticlesRepository(context);
        ThemesRepository = new ThemesRepository(context);
        TagsRepository = new TagsRepository(context);
    }

    public void AutoCommit(Action block)
    {
        block();
        Commit();
    }

    public void Commit()
    {
        _dbContext.SaveChanges();
    }
}