using News.DAL.Entities.Common;

namespace News.DAL.Entities;

public class Article : IdentifiableObject<int>
{
    public string Title { get; set; } = null!;
    public string Content { get; set; } = null!;
    
    public DateTime CreatedAt { get; set; }

    public ICollection<Tag> Tags { get; set; } = new List<Tag>();

    public string? ThemeId { get; set; }
    public Theme? Theme { get; set; }

    public int UserId { get; set; }
    public User User { get; set; }
}