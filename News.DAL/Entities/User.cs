using News.DAL.Entities.Common;

namespace News.DAL.Entities;

public class User : IdentifiableObject<int>
{
    public string FirstName { get; set; } = null!;
    public string SecondName { get; set; } = null!;
    public string Email { get; set; } = null!;
    public string Password { get; set; } = null!;

    public List<Article> Articles { get; set; }
}