using News.DAL.Entities.Common;

namespace News.DAL.Entities;

public class Tag : IdentifiableObject<string>
{
    public ICollection<Article> Articles { get; set; }
}