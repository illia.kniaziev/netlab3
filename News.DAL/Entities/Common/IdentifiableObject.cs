namespace News.DAL.Entities.Common;

public class IdentifiableObject<T>
{
    public T Id { get; set; }
}