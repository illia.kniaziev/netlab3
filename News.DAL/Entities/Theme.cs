using News.DAL.Entities.Common;

namespace News.DAL.Entities;

public class Theme : IdentifiableObject<string>
{
    public List<Article> Articles { get; set;  }
}