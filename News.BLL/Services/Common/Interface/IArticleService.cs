using News.BLL.DTO.Article;

namespace News.BLL.Services.Common.Interface;

public interface IArticleService
{
    ICollection<ArticleDto> GetAll(GetArticleFilterDto filterDto);
    ArticleDto GetById(int id);
    ArticleDto Create(NewArticleDto newArticleDto);
    ArticleDto Update(int id, UpdateArticleDto updateArticleDto);
    void Delete(int id);
}