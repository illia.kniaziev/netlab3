using News.BLL.DTO.Tag;

namespace News.BLL.Services.Common.Interface;

public interface ITagsService
{
    ICollection<TagDto> GetAll();
    TagDto Create(TagDto newTagDto);
    void Delete(string id);
}