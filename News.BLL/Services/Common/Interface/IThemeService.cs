using News.BLL.DTO.Theme;

namespace News.BLL.Services.Common.Interface;

public interface IThemeService
{
    ICollection<ThemeDto> GetAll();
    ThemeDto Create(ThemeDto newTagDto);
    void Delete(string id);
}