using News.BLL.DTO.User;

namespace News.BLL.Services.Common.Interface;

public interface IUserService
{
    ICollection<UserDto> GetAll();
    UserDto GetById(int id);
    UserDto Create(NewUserDto newUserDto);
    UserDto Update(int id, UpdateUserDto updateUserDto);
    void Delete(int id);
}