using AutoMapper;
using News.BLL.DTO.Article;
using News.BLL.Services.Common;
using News.BLL.Services.Common.Interface;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Services.Specific;

public class ArticleService : BaseService, IArticleService
{
    public ArticleService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { /* default */ }
    
    public ICollection<ArticleDto> GetAll(GetArticleFilterDto filterDto)
    {
        var articles = UnitOfWork.ArticlesRepository
            .GetFiltered(filterDto.UserId, filterDto.Theme, filterDto.Tags, filterDto.fromDate, filterDto.toDate);
        return Mapper.Map<ICollection<ArticleDto>>(articles);
    }

    public ArticleDto GetById(int id)
    {
        var article = GetUnwrappedArticleById(id);
        return Mapper.Map<ArticleDto>(article);
    }

    public ArticleDto Create(NewArticleDto newArticleDto)
    {
        var articleEntity = Mapper.Map<Article>(newArticleDto);

        if (!DoesUserExist(newArticleDto.UserId))
        {
            throw new KeyNotFoundException($"User {newArticleDto.UserId} not found");
        }
        
        if (!DoesThemeExist(newArticleDto.ThemeId))
        {
            throw new KeyNotFoundException($"Theme {newArticleDto.ThemeId} not found");
        }

        var tags = newArticleDto.Tags;

        foreach (var tag in tags)
        {
            var tagEntity = UnitOfWork.TagsRepository.GetById(tag);
            if (tagEntity is null)
            {
                throw new KeyNotFoundException($"Tag {tag} not found");
            }
            
            articleEntity.Tags.Add(tagEntity);
        }
        
        UnitOfWork.ArticlesRepository.Add(articleEntity);
        UnitOfWork.Commit();

        return Mapper.Map<ArticleDto>(articleEntity);
    }

    public ArticleDto Update(int id, UpdateArticleDto updateArticleDto)
    {
        var article = GetUnwrappedArticleById(id);
        if (updateArticleDto.ThemeId is not null)
        {
            if (!DoesThemeExist(updateArticleDto.ThemeId))
            {
                throw new KeyNotFoundException($"Theme {updateArticleDto.ThemeId} not found");
            }

            article.ThemeId = updateArticleDto.ThemeId;
        }
        
        var tags = updateArticleDto.Tags;

        if (tags is not null)
        {
            article.Tags = new List<Tag>();
            foreach (var tag in tags)
            {
                var tagEntity = UnitOfWork.TagsRepository.GetById(tag);
                if (tagEntity is null)
                {
                    throw new KeyNotFoundException($"Tag {tag} not found");
                }
            
                article.Tags.Add(tagEntity);
            }
        }

        article.Title = updateArticleDto.Title ?? article.Title;
        article.Content = updateArticleDto.Content ?? article.Content;

        UnitOfWork.AutoCommit(() => UnitOfWork.ArticlesRepository.Update(article));

        return Mapper.Map<ArticleDto>(article);
    }

    public void Delete(int id)
    {
        var article = GetUnwrappedArticleById(id);
        UnitOfWork.AutoCommit(() => UnitOfWork.ArticlesRepository.Delete(article));
    }

    private Article GetUnwrappedArticleById(int id)
    {
        var article = UnitOfWork.ArticlesRepository.GetById(id);
        if (article == null)
        {
            throw new KeyNotFoundException($"Article {id} not found");
        }

        return article;
    }

    private bool DoesThemeExist(string id)
    {
        var theme = UnitOfWork.ThemesRepository.GetById(id);
        return theme is not null;
    }

    private bool DoesUserExist(int id)
    {
        var theme = UnitOfWork.UsersRepository.GetById(id);
        return theme is not null;
    }
}