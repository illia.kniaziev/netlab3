using AutoMapper;
using News.BLL.DTO.Tag;
using News.BLL.Services.Common;
using News.BLL.Services.Common.Interface;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Services.Specific;

public class TagService : BaseService, ITagsService
{
    public TagService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { /* default */ }

    public ICollection<TagDto> GetAll()
    {
        var tags = UnitOfWork.TagsRepository.GetAll();
        return Mapper.Map<ICollection<TagDto>>(tags);
    }

    public TagDto Create(TagDto newTagDto)
    {
        var tag = UnitOfWork.TagsRepository.GetById(newTagDto.Id);
        if (tag is not null)
        {
            throw new ArgumentException("Tag already exist");
        }
        
        newTagDto.Id = newTagDto.Id.ToLower();
        var entity = Mapper.Map<Tag>(newTagDto);
        UnitOfWork.AutoCommit(() => UnitOfWork.TagsRepository.Add(entity));
        return newTagDto;
    }

    public void Delete(string id)
    {
        var tag = UnitOfWork.TagsRepository.GetById(id);
        if (tag is null)
        {
            throw new KeyNotFoundException($"Tag {id} not found");
        }
        UnitOfWork.AutoCommit(() => UnitOfWork.TagsRepository.Delete(tag));
    }
}