using AutoMapper;
using News.BLL.DTO.User;
using News.BLL.Services.Common;
using News.BLL.Services.Common.Interface;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Services.Specific;

public class UserService: BaseService, IUserService
{
    public UserService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { /* default */ }

    public ICollection<UserDto> GetAll()
    {
        var users = UnitOfWork.UsersRepository.GetAll();
        return Mapper.Map<ICollection<UserDto>>(users);
    }

    public UserDto GetById(int id)
    {
        var user = GetUnwrappedUserById(id);
        return Mapper.Map<UserDto>(user);
    }

    public UserDto Create(NewUserDto newUserDto)
    {
        var entity = Mapper.Map<User>(newUserDto);
        UnitOfWork.AutoCommit(() => UnitOfWork.UsersRepository.Add(entity));
        return Mapper.Map<UserDto>(entity);
    }

    public UserDto Update(int id, UpdateUserDto updateUserDto)
    {
        var user = GetUnwrappedUserById(id);
        user.FirstName = updateUserDto.FirstName ?? user.FirstName;
        user.SecondName = updateUserDto.SecondName ?? user.SecondName;
        user.Email = updateUserDto.Email ?? user.Email;
        user.Password = updateUserDto.Password ?? user.Password;

        UnitOfWork.AutoCommit(() => UnitOfWork.UsersRepository.Update(user));
        
        return Mapper.Map<UserDto>(user);
    }

    public void Delete(int id)
    {
        var user = GetUnwrappedUserById(id);
        UnitOfWork.AutoCommit(() => UnitOfWork.UsersRepository.Delete(user));
    }

    private User GetUnwrappedUserById(int id)
    {
        var user = UnitOfWork.UsersRepository.GetById(id);
        if (user == null)
        {
            throw new KeyNotFoundException($"User {id} not found");
        }

        return user;
    }
}