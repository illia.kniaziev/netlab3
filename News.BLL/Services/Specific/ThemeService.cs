using AutoMapper;
using News.BLL.DTO.Theme;
using News.BLL.Services.Common;
using News.BLL.Services.Common.Interface;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Services.Specific;

public class ThemeService : BaseService, IThemeService 
{
    public ThemeService(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { /* default */ }

    public ICollection<ThemeDto> GetAll()
    {
        var themes = UnitOfWork.ThemesRepository.GetAll();
        return Mapper.Map<ICollection<ThemeDto>>(themes);
    }

    public ThemeDto Create(ThemeDto newThemeDto)
    {
        var theme = UnitOfWork.ThemesRepository.GetById(newThemeDto.Id);
        if (theme is not null)
        {
            throw new ArgumentException("Theme already exist");
        }
        
        newThemeDto.Id = newThemeDto.Id.ToLower();
        var entity = Mapper.Map<Theme>(newThemeDto);
        UnitOfWork.AutoCommit(() => UnitOfWork.ThemesRepository.Add(entity));
        return newThemeDto;
    }

    public void Delete(string id)
    {
        var theme = UnitOfWork.ThemesRepository.GetById(id);
        if (theme is null)
        {
            throw new KeyNotFoundException($"Theme {id} not found");
        }
        UnitOfWork.AutoCommit(() => UnitOfWork.ThemesRepository.Delete(theme));
    }
}