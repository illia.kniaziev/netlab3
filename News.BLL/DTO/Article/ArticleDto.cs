using News.BLL.DTO.User;

namespace News.BLL.DTO.Article;

public class ArticleDto
{
    public int Id { get; set; }
    public string Title { get; set; }
    public string Content { get; set; }
    public ICollection<string> Tags { get; set; }
    public string? ThemeId { get; set; }
    public UserDto User { get; set; }
    
    public DateTime CreatedAt { get; set; }
}