namespace News.BLL.DTO.Article;

public class GetArticleFilterDto
{
    public int? UserId { get; set; }
    public string? Theme { get; set; }
    public ICollection<string>? Tags { get; set; }
    public DateTime? fromDate { get; set; }
    public DateTime? toDate { get; set; }
}