namespace News.BLL.DTO.Article;

public class UpdateArticleDto
{
    public string? Title { get; set; }
    public string? Content { get; set; }
    public ICollection<string>? Tags { get; set; }
    public string? ThemeId { get; set; }
}