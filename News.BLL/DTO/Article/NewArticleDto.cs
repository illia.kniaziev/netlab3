namespace News.BLL.DTO.Article;

public class NewArticleDto
{
    public string Title { get; set; } = null!;
    public string Content { get; set; } = null!;
    public ICollection<string> Tags { get; set; } = null!;
    public string ThemeId { get; set; } = null!;
    public int UserId { get; set; }
}