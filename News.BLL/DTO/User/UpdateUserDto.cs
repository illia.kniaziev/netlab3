namespace News.BLL.DTO.User;

public class UpdateUserDto
{
    public string? FirstName { get; set; }
    public string? SecondName { get; set; }
    public string? Email { get; set; }
    public string? Password { get; set; }
}