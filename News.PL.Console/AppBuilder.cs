using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using News.BLL.Services.Common.Interface;
using News.BLL.Services.Specific;
using News.DAL.Context;
using News.DAL.UnitOfWork;

namespace News.PL.Console;

public class AppBuilder
{
    public IServiceProvider Build()
    {
        var container = new ServiceCollection();

        container.AddSingleton<IUnitOfWork, UnitOfWork>();
        container.AddSingleton<IMapper, EntityMapper>();
        
        container.AddScoped<IUserService, UserService>();
        container.AddScoped<IArticleService, ArticleService>();
        container.AddScoped<ITagsService, TagService>();
        container.AddScoped<IThemeService, ThemeService>();
        
        container.AddSingleton<NewsDbContext>();
        
        return container.BuildServiceProvider();
    }
}