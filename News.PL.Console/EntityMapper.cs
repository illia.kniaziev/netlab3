using AutoMapper;
using News.BLL.DTO.Article;
using News.BLL.DTO.Tag;
using News.BLL.DTO.Theme;
using News.BLL.DTO.User;
using News.DAL.Entities;

namespace News.PL.Console;

public class EntityMapper : Mapper
{
    public EntityMapper() : base(new MapperConfiguration(EntityMapper.Configuration)) { /* default */ }

    private static void Configuration(IMapperConfigurationExpression cfg)
    {
        // user
        cfg.CreateMap<NewUserDto, User>();
        cfg.CreateMap<User, UserDto>().ReverseMap();

        // article
        cfg.CreateMap<NewArticleDto, Article>()
            .ForMember(dest => dest.Tags, act => act.Ignore());

        cfg.CreateMap<Tag, string>()
            .ConvertUsing(r=>r.Id);

        cfg.CreateMap<Article, ArticleDto>().ReverseMap();
        
        // tag
        cfg.CreateMap<Tag, TagDto>().ReverseMap();
        
        // theme
        cfg.CreateMap<Theme, ThemeDto>().ReverseMap();
    }
}