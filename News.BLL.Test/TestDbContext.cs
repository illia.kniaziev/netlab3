using Microsoft.EntityFrameworkCore;
using News.DAL.Context;
namespace News.BLL.Tests;

public class TestDbContext : NewsDbContext
{
    public TestDbContext(DbContextOptions<NewsDbContext> options) : base(options) { /* default */ }
    
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        
    }
}