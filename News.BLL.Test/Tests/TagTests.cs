using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using News.BLL.DTO.Article;
using News.BLL.DTO.Tag;
using News.BLL.DTO.Theme;
using News.BLL.DTO.User;
using News.BLL.Services.Specific;
using News.BLL.Tests;
using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Test.Tests;

public class TagTests
{
    private readonly TagService _tagService;
    
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public TagTests()
    {
        var cfg = new MapperConfiguration(cfg =>
        {
            // user
            cfg.CreateMap<NewUserDto, User>();
            cfg.CreateMap<User, UserDto>().ReverseMap();

            // article
            cfg.CreateMap<NewArticleDto, Article>()
                .ForMember(dest => dest.Tags, act => act.Ignore());

            cfg.CreateMap<Tag, string>()
                .ConvertUsing(r => r.Id);

            cfg.CreateMap<Article, ArticleDto>().ReverseMap();

            // tag
            cfg.CreateMap<Tag, TagDto>().ReverseMap();

            // theme
            cfg.CreateMap<Theme, ThemeDto>().ReverseMap();
        });

        _mapper = new Mapper(cfg);
        var builder = new DbContextOptionsBuilder<NewsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);

        _tagService = new TagService(_unitOfWork, _mapper);
    }

    [Fact]
    public void Create_Success()
    {
        var expected = new TagDto()
        {
            Id = "tennis"
        };

        _tagService.Create(expected);
        var tags = _tagService.GetAll();
        var actual = tags.FirstOrDefault();

        Assert.NotNull(actual);

        Assert.Equal(expected.Id, actual!.Id);
    }

    [Fact]
    public void CreateExisting_ArgumentException()
    {
        var tag1 = new TagDto()
        {
            Id = "swimming"
        };

        _tagService.Create(tag1);

        var tag2 = new TagDto()
        {
            Id = "swimming"
        };

        Assert.Throws<ArgumentException>(() => _tagService.Create(tag2));
    }

    [Fact]
    public void Delete_Success()
    {
        var expectedCountDeleted = 0;
        var expectedCountExisting = 1;
        var tag = new TagDto()
        {
            Id = "swimming"
        };
        _tagService.Create(tag);
        
        var tagsExisting = _tagService.GetAll();
        Assert.Equal(expectedCountExisting, tagsExisting.Count);
        
        _tagService.Delete("swimming");
        
        var tasDeleted = _tagService.GetAll();
        Assert.Equal(expectedCountDeleted, tasDeleted.Count);
    }
}