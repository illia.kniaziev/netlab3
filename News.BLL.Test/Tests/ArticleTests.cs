using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using News.BLL.DTO.Article;
using News.BLL.DTO.Tag;
using News.BLL.DTO.Theme;
using News.BLL.DTO.User;
using News.BLL.Services.Specific;
using News.BLL.Tests;
using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Test.Tests;

public class ArticleTests
{
    private readonly UserService _userService;
    private readonly ArticleService _articleService;
    private readonly ThemeService _themeService;
    private readonly TagService _tagService;

    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    private readonly NewArticleDto _testArticle = new NewArticleDto()
    {
        Title = "Title",
        Content = "Content",
        Tags = new List<string>() { "swimming", "tennis" },
        ThemeId = "sport",
        UserId = 1
    };

    public ArticleTests()
    {
        var cfg = new MapperConfiguration(cfg =>
        {
            // user
            cfg.CreateMap<NewUserDto, User>();
            cfg.CreateMap<User, UserDto>().ReverseMap();

            // article
            cfg.CreateMap<NewArticleDto, Article>()
                .ForMember(dest => dest.Tags, act => act.Ignore());

            cfg.CreateMap<Tag, string>()
                .ConvertUsing(r => r.Id);

            cfg.CreateMap<Article, ArticleDto>().ReverseMap();

            // tag
            cfg.CreateMap<Tag, TagDto>().ReverseMap();

            // theme
            cfg.CreateMap<Theme, ThemeDto>().ReverseMap();
        });

        _mapper = new Mapper(cfg);
        var builder = new DbContextOptionsBuilder<NewsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);

        _userService = new UserService(_unitOfWork, _mapper);
        _articleService = new ArticleService(_unitOfWork, _mapper);
        _themeService = new ThemeService(_unitOfWork, _mapper);
        _tagService = new TagService(_unitOfWork, _mapper);

        InitialSeed();
    }

    [Fact]
    public void GetAllByUser_MatchExpected()
    {
        var expectedUserId = 1;
        var expected = _testArticle;
        _articleService.Create(expected);

        var filter = new GetArticleFilterDto()
        {
            UserId = expectedUserId
        };

        var articles = _articleService.GetAll(filter);
        var actual = articles.FirstOrDefault();

        Assert.NotNull(actual);

        Assert.Equal(expectedUserId, actual!.User.Id);
    }

    [Fact]
    public void GetAllByTheme_MatchExpected()
    {
        var expectedTheme = "sport";
        var expected = _testArticle;
        _articleService.Create(expected);

        var filter = new GetArticleFilterDto()
        {
            Theme = expectedTheme
        };

        var articles = _articleService.GetAll(filter);
        var actual = articles.FirstOrDefault();

        Assert.NotNull(actual);

        Assert.Equal(expectedTheme, actual!.ThemeId);
    }

    [Fact]
    public void GetAllByTags_MatchExpected()
    {
        var expectedTag = "swimming";
        var expected = _testArticle;
        _articleService.Create(expected);

        var filter = new GetArticleFilterDto()
        {
            Tags = new List<string>() { expectedTag }
        };

        var articles = _articleService.GetAll(filter);
        var actual = articles.FirstOrDefault();

        Assert.NotNull(actual);

        Assert.True(actual!.Tags.Contains(expectedTag));
    }

    [Fact]
    public void GetById_Success()
    {
        var expectedId = 1;
        var expected = _testArticle;
        _articleService.Create(expected);

        var actual = _articleService.GetById(1);

        Assert.NotNull(actual);
        Assert.Equal(expectedId, actual.Id);
    }

    [Fact]
    public void GetById_KeyNotFoundException()
    {
        Assert.Throws<KeyNotFoundException>(()=>_articleService.GetById(1223));
    }

    [Fact]
    public void Create_ThemeNotExist_KeyNotFoundException()
    {
        var article = new NewArticleDto()
        {
            Title = "Title",
            Content = "Content",
            Tags = new List<string>() { "swimming", "tennis" },
            ThemeId = "cooking",
            UserId = 1
        };
        Assert.Throws<KeyNotFoundException>(() => _articleService.Create(article));
    }
    
    [Fact]
    public void Create_TagNotExist_KeyNotFoundException()
    {
        var article = new NewArticleDto()
        {
            Title = "Title",
            Content = "Content",
            Tags = new List<string>() { "football", "tennis" },
            ThemeId = "sport",
            UserId = 1
        };
        Assert.Throws<KeyNotFoundException>(() => _articleService.Create(article));
    }
    
    [Fact]
    public void Create_UserNotExist_KeyNotFoundException()
    {
        var article = new NewArticleDto()
        {
            Title = "Title",
            Content = "Content",
            Tags = new List<string>() { "swimming", "tennis" },
            ThemeId = "sport",
            UserId = 22
        };
        Assert.Throws<KeyNotFoundException>(() => _articleService.Create(article));
    }

    [Fact]
    public void Update_Success()
    {
        var initial = _testArticle;
        _articleService.Create(initial);

        var expected = new UpdateArticleDto()
        {
            Title = "t",
            Content = "c",
            Tags = new List<string>() { "swimming" },
            ThemeId = "education"
        };

        var actual = _articleService.Update(1, expected);
        
        Assert.Equal(expected.Title, actual.Title);
        Assert.Equal(expected.Content, actual.Content);
        Assert.Equal(expected.ThemeId, actual.ThemeId);
        Assert.Equal(expected.Tags, actual.Tags);
    }

    [Fact]
    public void Update_ThemeNotExist_KeyNotFoundException()
    {
        var initial = _testArticle;
        _articleService.Create(initial);

        var expected = new UpdateArticleDto()
        {
            Title = "t",
            Content = "c",
            Tags = new List<string>() { "swimming" },
            ThemeId = "null :)"
        };

        Assert.Throws<KeyNotFoundException>(()=>_articleService.Update(1, expected));
    }
    
    [Fact]
    public void Update_TagNotExist_KeyNotFoundException()
    {
        var initial = _testArticle;
        _articleService.Create(initial);

        var expected = new UpdateArticleDto()
        {
            Title = "t",
            Content = "c",
            Tags = new List<string>() { "rugby" },
            ThemeId = "education"
        };

        Assert.Throws<KeyNotFoundException>(()=>_articleService.Update(1, expected));
    }

    [Fact]
    public void Delete_Success()
    {
        var expectedCountDeleted = 0;
        var expectedCountExisting = 1;
        var initial = _testArticle;
        _articleService.Create(initial);
        
        var articlesExisting = _articleService.GetAll(new GetArticleFilterDto());
        Assert.Equal(expectedCountExisting, articlesExisting.Count);
        
        _articleService.Delete(1);
        
        var articlesDeleted = _articleService.GetAll(new GetArticleFilterDto());
        Assert.Equal(expectedCountDeleted, articlesDeleted.Count);
    }

    [Fact]
    public void Delete_KeyNotFoundException()
    {
        Assert.Throws<KeyNotFoundException>(() => _articleService.Delete(939));
    }

    private void InitialSeed()
    {
        var theme1 = new ThemeDto()
        {
            Id = "sport"
        };
        _themeService.Create(theme1);
        
        var theme2 = new ThemeDto()
        {
            Id = "education"
        };
        _themeService.Create(theme2);

        var tag1 = new TagDto()
        {
            Id = "tennis"
        };
        _tagService.Create(tag1);
        
        var tag2 = new TagDto()
        {
            Id = "swimming"
        };
        _tagService.Create(tag2);
        
        var user = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };
        
        _userService.Create(user);
    }
}