using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using News.BLL.DTO.Article;
using News.BLL.DTO.Tag;
using News.BLL.DTO.Theme;
using News.BLL.DTO.User;
using News.BLL.Services.Specific;
using News.BLL.Tests;
using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Test.Tests;

public class ThemeTests
{
    private readonly UserService _userService;
    private readonly ArticleService _articleService;
    private readonly ThemeService _themeService;
    
    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public ThemeTests()
    {
        var cfg = new MapperConfiguration(cfg =>
        {
            // user
            cfg.CreateMap<NewUserDto, User>();
            cfg.CreateMap<User, UserDto>().ReverseMap();

            // article
            cfg.CreateMap<NewArticleDto, Article>()
                .ForMember(dest => dest.Tags, act => act.Ignore());

            cfg.CreateMap<Tag, string>()
                .ConvertUsing(r => r.Id);

            cfg.CreateMap<Article, ArticleDto>().ReverseMap();

            // tag
            cfg.CreateMap<Tag, TagDto>().ReverseMap();

            // theme
            cfg.CreateMap<Theme, ThemeDto>().ReverseMap();
        });

        _mapper = new Mapper(cfg);
        var builder = new DbContextOptionsBuilder<NewsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);

        _userService = new UserService(_unitOfWork, _mapper);
        _themeService = new ThemeService(_unitOfWork, _mapper);
        _articleService = new ArticleService(_unitOfWork, _mapper);
    }

    [Fact]
    public void Create_Success()
    {
        var expected = new ThemeDto()
        {
            Id = "sport"
        };

        _themeService.Create(expected);
        var themes = _themeService.GetAll();
        var actual = themes.FirstOrDefault();

        Assert.NotNull(actual);

        Assert.Equal(expected.Id, actual!.Id);
    }

    [Fact]
    public void CreateExisting_ArgumentException()
    {
        var theme1 = new ThemeDto()
        {
            Id = "sport"
        };

        _themeService.Create(theme1);

        var theme2 = new ThemeDto()
        {
            Id = "sport"
        };

        Assert.Throws<ArgumentException>(() => _themeService.Create(theme2));
    }

    [Fact]
    public void Delete_Success()
    {
        var expectedCountDeleted = 0;
        var expectedCountExisting = 1;
        var theme = new ThemeDto()
        {
            Id = "sport"
        };
        _themeService.Create(theme);
        
        var themesExisting = _themeService.GetAll();
        Assert.Equal(expectedCountExisting, themesExisting.Count);
        
        _themeService.Delete("sport");
        
        var themesDeleted = _themeService.GetAll();
        Assert.Equal(expectedCountDeleted, themesDeleted.Count);
    }

    [Fact]
    public void CreateArticle_DeleteTheme_ArticleThemeSetToNull()
    {
        var theme = new ThemeDto()
        {
            Id = "sport"
        };
        _themeService.Create(theme);

        var user = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };
        _userService.Create(user);
        
        var article = new NewArticleDto()
        {
            Title = "Title",
            Content = "Content",
            Tags = new List<string>(),
            ThemeId = "sport",
            UserId = 1
        };
        _articleService.Create(article);
        _themeService.Delete("sport");

        var actual = _articleService.GetById(1);

        Assert.Null(actual.ThemeId);
    }

}