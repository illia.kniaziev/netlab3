using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using News.BLL.DTO.Article;
using News.BLL.DTO.Tag;
using News.BLL.DTO.Theme;
using News.BLL.DTO.User;
using News.BLL.Services.Specific;
using News.BLL.Tests;
using News.DAL.Context;
using News.DAL.Entities;
using News.DAL.UnitOfWork;

namespace News.BLL.Test.Tests;

public class UserTests
{
    private readonly UserService _userService;
    private readonly ArticleService _articleService;
    private readonly ThemeService _themeService;

    private readonly IUnitOfWork _unitOfWork;
    private readonly IMapper _mapper;
    private readonly TestDbContext _context;

    public UserTests()
    {
        var cfg = new MapperConfiguration(cfg =>
        {
            // user
            cfg.CreateMap<NewUserDto, User>();
            cfg.CreateMap<User, UserDto>().ReverseMap();

            // article
            cfg.CreateMap<NewArticleDto, Article>()
                .ForMember(dest => dest.Tags, act => act.Ignore());

            cfg.CreateMap<Tag, string>()
                .ConvertUsing(r => r.Id);

            cfg.CreateMap<Article, ArticleDto>().ReverseMap();

            // tag
            cfg.CreateMap<Tag, TagDto>().ReverseMap();

            // theme
            cfg.CreateMap<Theme, ThemeDto>().ReverseMap();
        });

        _mapper = new Mapper(cfg);
        var builder = new DbContextOptionsBuilder<NewsDbContext>();
        builder.UseInMemoryDatabase(Guid.NewGuid().ToString());
        builder.ConfigureWarnings(b => b.Ignore(InMemoryEventId.TransactionIgnoredWarning));
        builder.EnableSensitiveDataLogging();

        _context = new TestDbContext(builder.Options);
        _unitOfWork = new UnitOfWork(_context);

        _userService = new UserService(_unitOfWork, _mapper);
        _articleService = new ArticleService(_unitOfWork, _mapper);
        _themeService = new ThemeService(_unitOfWork, _mapper);
    }

    [Fact]
    public void GetAll_Success()
    {
        var expectedCount = 1;
        var expected = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };
        
        _userService.Create(expected);

        var users = _userService.GetAll();
        var actual = users.FirstOrDefault();
        
        Assert.Equal(expectedCount, users.Count);
        Assert.NotNull(actual);
        
        Assert.Equal(expected.FirstName, actual!.FirstName);
        Assert.Equal(expected.SecondName, actual!.SecondName);
        Assert.Equal(expected.Email, actual!.Email);
    }

    [Fact]
    public void CreateUser_SavedAsIs_Success()
    {
        var expected = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };
        
        var actual = _userService.Create(expected);
        
        Assert.Equal(expected.FirstName, actual.FirstName);
        Assert.Equal(expected.SecondName, actual.SecondName);
        Assert.Equal(expected.Email, actual.Email);
    }

    [Fact]
    public void GetUserById_Success()
    {
        var expected = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };

        _userService.Create(expected);
        
        var actual = _userService.GetById(1);
        
        Assert.Equal(expected.FirstName, actual.FirstName);
        Assert.Equal(expected.SecondName, actual.SecondName);
        Assert.Equal(expected.Email, actual.Email);
    }
    
    [Fact]
    public void GetUserById_KeyNotFoundException()
    {
        var expected = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };

        _userService.Create(expected);
        Assert.Throws<KeyNotFoundException>(()=>_userService.GetById(22));
    }

    [Fact]
    public void UpdateUser_Success()
    {
        var initial = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };

        var expected = new UpdateUserDto()
        {
            FirstName = "Eugen1",
            SecondName = "22",
            Email = "00sksksk@skd.com"
        };

        _userService.Create(initial);
        var actual = _userService.Update(1, expected);
        
        Assert.Equal(expected.FirstName, actual.FirstName);
        Assert.Equal(expected.SecondName, actual.SecondName);
        Assert.Equal(expected.Email, actual.Email);
    }

    [Fact]
    public void UpdateUser_KeyNotFoundException()
    {
        var expected = new UpdateUserDto()
        {
            FirstName = "Eugen1",
            SecondName = "22",
            Email = "00sksksk@skd.com"
        };

        Assert.Throws<KeyNotFoundException>(() => _userService.Update(939, expected));
    }

    [Fact]
    public void DeleteUser_Success()
    {
        var expectedCountDeleted = 0;
        var expectedCountExisting = 1;
        var expected = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };
        
        _userService.Create(expected);
        
        var usersExisting = _userService.GetAll();
        Assert.Equal(expectedCountExisting, usersExisting.Count);
        
        _userService.Delete(1);
        
        var usersDeleted = _userService.GetAll();
        Assert.Equal(expectedCountDeleted, usersDeleted.Count);
    }

    [Fact]
    public void DeleteUser_KeyNotFoundException()
    {
        Assert.Throws<KeyNotFoundException>(() => _userService.Delete(939));
    }

    [Fact]
    public void DeleteUser_ArticlesRemoved()
    {
        var expectedCountOnDelete = 0;
        var expectedCountOnCreate = 1;
        
        var theme = new ThemeDto()
        {
            Id = "sport"
        };
        _themeService.Create(theme);

        var user = new NewUserDto()
        {
            FirstName = "Eugen",
            SecondName = "Olsssewe",
            Email = "sksksk@skd.com",
            Password = "sdsdwww232**w"
        };
        _userService.Create(user);
        
        var article = new NewArticleDto()
        {
            Title = "Title",
            Content = "Content",
            Tags = new List<string>(),
            ThemeId = "sport",
            UserId = 1
        };
        _articleService.Create(article);

        var articlesOnCreate = _articleService.GetAll(new GetArticleFilterDto());
        _userService.Delete(1);
        
        var articlesOnDelete = _articleService.GetAll(new GetArticleFilterDto());
        
        Assert.Equal(expectedCountOnCreate, articlesOnCreate.Count);
        Assert.Equal(expectedCountOnDelete, articlesOnDelete.Count);
    }
}