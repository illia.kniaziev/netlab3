using FluentValidation.AspNetCore;
using Microsoft.OpenApi.Models;
using News.PL.WebAPI.Extensions;
using News.PL.WebAPI.Middleware;


namespace News.PL.WebAPI.App;

public class Startup
{
    public Startup(IConfiguration configuration)
    {
        Configuration = configuration;
    }

    public IConfiguration Configuration { get; }
    
    public void ConfigureServices(IServiceCollection services)
    {
        services.AddControllers();
        services.AddFluentValidationAutoValidation();
            
        services.AddSwaggerGen(c =>
        {
            c.CustomSchemaIds(type => type.ToString());
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "WebAPI", Version = "v1" });
        });
        
        services.RegisterServices();
        services.RegisterValidators();
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
            app.UseSwagger();
            app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "WebAPI v1"));
        }

        app.UseRouting();

        app.UseMiddleware<ExceptionMappingMiddleware>();

        app.UseEndpoints(endpoints => endpoints.MapControllers());
    }
}