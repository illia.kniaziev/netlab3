using System.Text.Json;
using News.BLL.DTO.Error;

namespace News.PL.WebAPI.Middleware;

public class ExceptionMappingMiddleware
{
    private readonly RequestDelegate _delegate;

    public ExceptionMappingMiddleware(RequestDelegate aDelegate)
    {
        _delegate = aDelegate;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            await _delegate(context);
        }
        catch (Exception e)
        {
            await MapException(context, e);
        }
    }

    private static Task MapException(HttpContext context, Exception exception)
    {
        context.Response.ContentType = "application/json";
        context.Response.StatusCode = exception switch
        {
            KeyNotFoundException => 404,
            ArgumentException => 400,
            _ => 500,
        };

        var error = new ErrorDto(exception.Message);
        var json = JsonSerializer.Serialize(error);

        return context.Response.WriteAsync(json);
    }
}