using AutoMapper;
using FluentValidation;
using News.BLL.DTO.Article;
using News.BLL.DTO.Tag;
using News.BLL.DTO.Theme;
using News.BLL.DTO.User;
using News.BLL.Services.Common.Interface;
using News.BLL.Services.Specific;
using News.DAL.Context;
using News.DAL.UnitOfWork;
using News.PL.WebAPI.App;
using News.PL.WebAPI.Validation.Article;
using News.PL.WebAPI.Validation.Tag;
using News.PL.WebAPI.Validation.Theme;
using News.PL.WebAPI.Validation.User;

namespace News.PL.WebAPI.Extensions;

public static class ServiceExtension
{
    public static void RegisterServices(this IServiceCollection container)
    {
        container.AddSingleton<IUnitOfWork, UnitOfWork>();
        container.AddSingleton<IMapper, EntityMapper>();
        
        container.AddScoped<IUserService, UserService>();
        container.AddScoped<IArticleService, ArticleService>();
        container.AddScoped<ITagsService, TagService>();
        container.AddScoped<IThemeService, ThemeService>();
        
        container.AddSingleton<NewsDbContext>();
    }

    public static void RegisterValidators(this IServiceCollection container)
    {
        container.AddSingleton<IValidator<NewUserDto>, NewUserDtoValidator>();
        container.AddSingleton<IValidator<UpdateUserDto>, UpdateUserDtoValidator>();
        
        container.AddSingleton<IValidator<NewArticleDto>, NewArticleDtoValidator>();
        container.AddSingleton<IValidator<UpdateArticleDto>, UpdateArticleDtoValidator>();
        
        container.AddSingleton<IValidator<TagDto>, TagDtoValidator>();
        
        container.AddSingleton<IValidator<ThemeDto>, ThemeDtoValidator>();
    }
}