using FluentValidation;
using News.BLL.DTO.Theme;

namespace News.PL.WebAPI.Validation.Theme;

public class ThemeDtoValidator : AbstractValidator<ThemeDto>
{
    public ThemeDtoValidator()
    {
        RuleFor(t=>t.Id)
            .Must(c=>!string.IsNullOrEmpty(c))
            .WithMessage("Id must not be empty");
            
        RuleFor(t=>t.Id)
            .Must(c=>!string.IsNullOrWhiteSpace(c))
            .WithMessage("Id must not be a whitespace");
    }
}