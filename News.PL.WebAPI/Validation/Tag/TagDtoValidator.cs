using FluentValidation;
using News.BLL.DTO.Tag;

namespace News.PL.WebAPI.Validation.Tag;

public class TagDtoValidator : AbstractValidator<TagDto>
{
    public TagDtoValidator()
    {
        RuleFor(t=>t.Id)
            .Must(c=>!string.IsNullOrEmpty(c))
            .WithMessage("Id must not be empty");
            
        RuleFor(t=>t.Id)
            .Must(c=>!string.IsNullOrWhiteSpace(c))
            .WithMessage("Id must not be a whitespace");
    }
}