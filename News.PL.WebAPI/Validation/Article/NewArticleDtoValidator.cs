using FluentValidation;
using News.BLL.DTO.Article;

namespace News.PL.WebAPI.Validation.Article;

public class NewArticleDtoValidator : AbstractValidator<NewArticleDto>
{
    public NewArticleDtoValidator()
    {
        RuleFor(a => a.Title)
            .NotEmpty()
            .WithMessage("Title must not be empty");

        RuleFor(a => a.Title)
            .MaximumLength(100)
            .WithMessage("Title length must be lower then 100");
        
        RuleFor(a => a.Title)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .WithMessage("Title must not be a whitespace");
        
        
        RuleFor(a => a.Content)
            .NotEmpty()
            .WithMessage("Title must not be empty");

        RuleFor(a => a.Content)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .WithMessage("Title must not be a whitespace");
    }
}