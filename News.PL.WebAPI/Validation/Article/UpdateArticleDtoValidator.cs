using FluentValidation;
using News.BLL.DTO.Article;

namespace News.PL.WebAPI.Validation.Article;

public class UpdateArticleDtoValidator : AbstractValidator<UpdateArticleDto>
{
    public UpdateArticleDtoValidator()
    {
        RuleFor(a => a.Title)
            .NotEmpty()
            .When(u => u.Title is not null)
            .WithMessage("Title must not be empty");

        RuleFor(a => a.Title)
            .MaximumLength(100)
            .When(u => u.Title is not null)
            .WithMessage("Title length must be lower then 100");
        
        RuleFor(a => a.Title)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .When(u => u.Title is not null)
            .WithMessage("Title must not be a whitespace");
        
        
        RuleFor(a => a.Content)
            .NotEmpty()
            .When(u => u.Content is not null)
            .WithMessage("Title must not be empty");

        RuleFor(a => a.Content)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .When(u => u.Content is not null)
            .WithMessage("Title must not be a whitespace");
    }
}