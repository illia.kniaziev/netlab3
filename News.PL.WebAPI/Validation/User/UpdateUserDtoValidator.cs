using FluentValidation;
using News.BLL.DTO.User;

namespace News.PL.WebAPI.Validation.User;

public class UpdateUserDtoValidator : AbstractValidator<UpdateUserDto>
{
    public UpdateUserDtoValidator()
    {
        RuleFor(u => u.FirstName)
            .NotEmpty()
            .When(u => u.FirstName is not null)
            .WithMessage("First name must not be empty");

        RuleFor(u => u.FirstName)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .When(u => u.FirstName is not null)
            .WithMessage("Whitespace first name is not allowed");
        
        RuleFor(u => u.FirstName)
            .MaximumLength(50)
            .When(u => u.FirstName is not null)
            .WithMessage("First name length must be equal or less than 50 ch");
        
        
        RuleFor(u => u.SecondName)
            .NotEmpty()
            .When(u => u.SecondName is not null)
            .WithMessage("Second name must not be empty");

        RuleFor(u => u.SecondName)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .When(u => u.SecondName is not null)
            .WithMessage("Whitespace second name is not allowed");
        
        RuleFor(u => u.SecondName)
            .MaximumLength(50)
            .When(u => u.SecondName is not null)
            .WithMessage("Second name length must be equal or less than 50 ch");



        RuleFor(u => u.Email)
            .EmailAddress()
            .When(u => u.Email is not null)
            .WithMessage("Not valid email given");

        
        RuleFor(u => u.Password)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .When(u => u.Password is not null)
            .WithMessage("Whitespace password name is not allowed");
        
        RuleFor(u => u.Password)
            .MinimumLength(8)
            .When(u => u.Password is not null)
            .WithMessage("Password length mast be at least 8 ch");
            
        RuleFor(u => u.Password)
            .MaximumLength(50)
            .When(u => u.Password is not null)
            .WithMessage("Password length mast be equal or less then 50 ch");
    }
}