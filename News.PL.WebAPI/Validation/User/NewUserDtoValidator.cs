using FluentValidation;
using News.BLL.DTO.User;

namespace News.PL.WebAPI.Validation.User;

public class NewUserDtoValidator : AbstractValidator<NewUserDto>
{
    public NewUserDtoValidator()
    {
        RuleFor(u => u.FirstName)
            .NotEmpty()
            .WithMessage("First name must not be empty");

        RuleFor(u => u.FirstName)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .WithMessage("Whitespace first name is not allowed");
        
        RuleFor(u => u.FirstName)
            .MaximumLength(50)
            .WithMessage("First name length must be equal or less than 50 ch");
        
        
        RuleFor(u => u.SecondName)
            .NotEmpty()
            .WithMessage("Second name must not be empty");

        RuleFor(u => u.SecondName)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .WithMessage("Whitespace second name is not allowed");
        
        RuleFor(u => u.SecondName)
            .MaximumLength(50)
            .WithMessage("Second name length must be equal or less than 50 ch");



        RuleFor(u => u.Email)
            .EmailAddress()
            .WithMessage("Not valid email given");

        
        RuleFor(u => u.Password)
            .Must(userName => !userName.All(char.IsWhiteSpace))
            .WithMessage("Whitespace password name is not allowed");
        
        RuleFor(u => u.Password)
            .MinimumLength(8)
            .WithMessage("Password length mast be at least 8 ch");
            
        RuleFor(u => u.Password)
            .MaximumLength(50)
            .WithMessage("Password length mast be equal or less then 50 ch");
    }
}