using Microsoft.AspNetCore.Mvc;
using News.BLL.DTO.Article;
using News.BLL.Services.Common.Interface;

namespace News.PL.WebAPI.Controllers;

[ApiController]
[Route("api/articles")]
public class ArticlesController : ControllerBase
{
    private readonly IArticleService _articleService;

    public ArticlesController(IArticleService articleService)
    {
        _articleService = articleService;
    }

    [HttpGet]
    public ActionResult<ICollection<ArticleDto>> Get(
        [FromQuery] int? userId, 
        [FromQuery] string? theme, 
        [FromQuery] List<string> tags, 
        [FromQuery] DateTime? fromDate, 
        [FromQuery] DateTime? toDate)
    {
        var filterDto = new GetArticleFilterDto
        {
            UserId = userId,
            Theme = theme,
            Tags = tags,
            fromDate = fromDate,
            toDate = toDate
        };
        
        return Ok(_articleService.GetAll(filterDto));
    }

    [HttpGet("{id}")]
    public ActionResult<ArticleDto> GetById(int id)
    {
        return Ok(_articleService.GetById(id));
    }

    [HttpPost]
    public ActionResult<ArticleDto> Create([FromBody] NewArticleDto articleDto)
    {
        return Ok(_articleService.Create(articleDto));
    }

    [HttpPut("{id}")]
    public ActionResult<ArticleDto> Update(int id, [FromBody] UpdateArticleDto updateArticleDto)
    {
        return Ok(_articleService.Update(id, updateArticleDto));
    }

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        _articleService.Delete(id);
        return NoContent();
    }
}