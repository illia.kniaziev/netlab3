using Microsoft.AspNetCore.Mvc;
using News.BLL.DTO.Theme;
using News.BLL.Services.Common.Interface;

namespace News.PL.WebAPI.Controllers;

[ApiController]
[Route("api/themes")]
public class ThemesController : ControllerBase
{
    private readonly IThemeService _themeService;

    public ThemesController(IThemeService themeService)
    {
        _themeService = themeService;
    }

    [HttpGet]
    public ActionResult<ICollection<ThemeDto>> Get()
    {
        return Ok(_themeService.GetAll());
    }

    [HttpPost]
    public ActionResult<ThemeDto> Create([FromBody] ThemeDto tagDto)
    {
        return Ok(_themeService.Create(tagDto));
    }
    
    [HttpDelete("{id}")]
    public ActionResult Delete(string id)
    {
        _themeService.Delete(id);
        return NoContent();
    }
}