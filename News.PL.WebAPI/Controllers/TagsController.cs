using Microsoft.AspNetCore.Mvc;
using News.BLL.DTO.Tag;
using News.BLL.Services.Common.Interface;

namespace News.PL.WebAPI.Controllers;

[ApiController]
[Route("api/tags")]
public class TagsController : ControllerBase
{
    private readonly ITagsService _tagsService;

    public TagsController(ITagsService tagsService)
    {
        _tagsService = tagsService;
    }

    [HttpGet]
    public ActionResult<ICollection<TagDto>> Get()
    {
        return Ok(_tagsService.GetAll());
    }

    [HttpPost]
    public ActionResult<TagDto> Create([FromBody] TagDto tagDto)
    {
        return Ok(_tagsService.Create(tagDto));
    }
    
    [HttpDelete("{id}")]
    public ActionResult Delete(string id)
    {
        _tagsService.Delete(id);
        return NoContent();
    }
}