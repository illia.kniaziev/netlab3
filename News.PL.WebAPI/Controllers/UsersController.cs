using Microsoft.AspNetCore.Mvc;
using News.BLL.DTO.Article;
using News.BLL.DTO.User;
using News.BLL.Services.Common.Interface;

namespace News.PL.WebAPI.Controllers;

[ApiController]
[Route("api/users")]
public class UsersController : ControllerBase
{
    private readonly IUserService _userService;

    public UsersController(IUserService userService)
    {
        _userService = userService;
    }

    [HttpGet]
    public ActionResult<ICollection<UserDto>> Get()
    {
        return Ok(_userService.GetAll());
    }

    [HttpGet("{id}")]
    public ActionResult<UserDto> GetById(int id)
    {
        return Ok(_userService.GetById(id));
    }

    [HttpPost]
    public ActionResult<ArticleDto> Create([FromBody] NewUserDto userDto)
    {
        return Ok(_userService.Create(userDto));
    }

    [HttpPut("{id}")]
    public ActionResult<UpdateArticleDto> Update(int id, [FromBody] UpdateUserDto updateUserDto)
    {
        return Ok(_userService.Update(id, updateUserDto));
    } 

    [HttpDelete("{id}")]
    public ActionResult Delete(int id)
    {
        _userService.Delete(id);
        return NoContent();
    }
}